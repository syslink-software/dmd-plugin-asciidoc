# dmd-plugin-asciidoc

This is a work in experimental plugin for `jsdoc2md` to produce AsciiDoc output.

To use a plug-in in your project, first install it as a development dependency:

```shell
npm install git+https://bitbucket.org/syslink-software/dmd-plugin-asciidoc.git --save-dev
```

Then pass the plug-in name to `jsdoc2md` or `dmd`:

```shell
jsdoc2md --plugin @avantra/dmd-plugin-asciidoc lib/*.js
```
